package nz.co.digitalarmoury.posture.ui.learnMore.di;

import nz.co.digitalarmoury.posture.ui.learnMore.LearnMoreActivity;

import dagger.Module;

@Module
public class LearnMoreModule {

    private LearnMoreActivity mInjectee;

    public LearnMoreModule(LearnMoreActivity injectee) {
        this.mInjectee = injectee;
    }

}
