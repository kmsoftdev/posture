package nz.co.digitalarmoury.posture.ui.settings;

import nz.co.digitalarmoury.posture.ui.BaseView;

public interface SettingsView extends BaseView {

    void serviceStatusUpdated(boolean mActivated);

    void restoreUserSettings(boolean isUsingOverlay,
                             boolean isUsingDialog,
                             boolean dialogStrictMode,
                             boolean isUsingVibration,
                             boolean isUsingFaceRecognition);


}
