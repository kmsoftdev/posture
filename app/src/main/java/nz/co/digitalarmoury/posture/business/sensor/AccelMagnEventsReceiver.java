package nz.co.digitalarmoury.posture.business.sensor;

import android.app.KeyguardManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import nz.co.digitalarmoury.posture.business.bus.Bus;
import nz.co.digitalarmoury.posture.business.prefs.PrefsRepo;
import nz.co.digitalarmoury.posture.model.sensors.SensorEventsWrapper;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import rx.subscriptions.Subscriptions;

@Singleton
public class AccelMagnEventsReceiver implements SensorEventsReceiver {

    private final Bus mBus;
    private final SensorManager mSensorManager;
    private final Sensor mAccelerometer;
    private final Sensor mMagnetometer;
    private final SensorEventsHandler mSensorEventsHandler;
    private final PrefsRepo mPrefsRepo;
    private final KeyguardManager mKeyguardManager;
    private final CompositeSubscription subs;
    private Subscription mSubscribedToPendingTiltChange;

    @Inject
    public AccelMagnEventsReceiver(Bus bus,
                                   SensorManager sensorManager,
                                   @Named("accelerometer") Sensor accelerometer,
                                   @Named("magnetometer") Sensor magnetometer,
                                   SensorEventsHandler sensorEventsHandler,
                                   PrefsRepo prefsRepo,
                                   KeyguardManager keyguardManager) {
        mBus = bus;
        mSensorManager = sensorManager;
        mAccelerometer = accelerometer;
        mMagnetometer = magnetometer;
        mSensorEventsHandler = sensorEventsHandler;
        mPrefsRepo = prefsRepo;
        mKeyguardManager = keyguardManager;
        subs = new CompositeSubscription();
    }

    @Override
    public void setTiltDetection(boolean activated) {
        if (activated) {
            Observable<Boolean> tiltValidated = startValidatingTilt()
                    .onErrorResumeNext(throwable -> startValidatingTilt())
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread());

            if (mSubscribedToPendingTiltChange != null && !mSubscribedToPendingTiltChange.isUnsubscribed()) {
                mSubscribedToPendingTiltChange.unsubscribe();
            }
            mSubscribedToPendingTiltChange = tiltValidated
                    .subscribe(mBus::publishTiltValidatedEvent, Throwable::printStackTrace);
            subs.add(mSubscribedToPendingTiltChange);
        }
        else {
            subs.clear();
        }
    }

    private Observable<Boolean> startValidatingTilt() {
        return Observable.interval(mPrefsRepo.getSensorInterval(), mPrefsRepo.getSensorInterval(), TimeUnit.SECONDS)
                         .takeWhile(tick -> isDeviceLocked())
                         .repeat()
                         .flatMap(tick -> registerAccelerometerValuesListener())
                         .flatMap(mSensorEventsHandler::validateCurrentTilt)
                         .distinctUntilChanged();
    }

    private boolean isDeviceLocked() {
        return !mKeyguardManager.inKeyguardRestrictedInputMode();
    }

    private Observable<SensorEventsWrapper> registerAccelerometerValuesListener() {

        return Observable.zip(getAccelerometerSensorEvent(mSensorManager, mAccelerometer),
                              getMagnetometerSensorEvent(mSensorManager, mMagnetometer),
                              SensorEventsWrapper::new);

    }

    private Observable<SensorEvent> getAccelerometerSensorEvent(SensorManager sensorManager, Sensor accelerometer) {
        return Observable.create(subscriber -> {
            SensorEventListener accelerometerListener = new SensorEventListener() {
                @Override
                public void onSensorChanged(SensorEvent sensorEvent) {
                    subscriber.onNext(sensorEvent);
                    subscriber.onCompleted();
                }

                @Override
                public void onAccuracyChanged(Sensor sensor, int i) {

                }
            };
            sensorManager.registerListener(accelerometerListener, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);

            subscriber.add(Subscriptions.create(() -> {
                sensorManager.unregisterListener(accelerometerListener);
            }));

        }).sample(16, TimeUnit.MILLISECONDS).cast(SensorEvent.class);
    }

    private Observable<SensorEvent> getMagnetometerSensorEvent(SensorManager sensorManager, Sensor magnetometer) {
        return Observable.create(subscriber -> {
            SensorEventListener magnetometerListener = new SensorEventListener() {
                @Override
                public void onSensorChanged(SensorEvent sensorEvent) {
                    subscriber.onNext(sensorEvent);
                    subscriber.onCompleted();
                }

                @Override
                public void onAccuracyChanged(Sensor sensor, int i) {

                }
            };
            sensorManager.registerListener(magnetometerListener, magnetometer, SensorManager.SENSOR_DELAY_NORMAL);

            subscriber.add(Subscriptions.create(() -> {
                sensorManager.unregisterListener(magnetometerListener);
            }));
        }).sample(16, TimeUnit.MILLISECONDS).cast(SensorEvent.class);
    }
}
