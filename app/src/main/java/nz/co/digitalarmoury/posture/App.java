package nz.co.digitalarmoury.posture;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import nz.co.digitalarmoury.posture.di.AppComponent;
import nz.co.digitalarmoury.posture.di.DaggerAppComponent;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

import nz.co.digitalarmoury.posture.di.AppModule;

public class App extends Application {
    private AppComponent mAppComponent;
    private RefWatcher mRefWatcher;

    public AppComponent getAppComponent() {
        return this.mAppComponent;
    }

    public RefWatcher getRefWatcher() {
        return this.mRefWatcher;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        this.mRefWatcher = LeakCanary.install(this);
        this.mAppComponent = buildComponent();
    }

    protected AppComponent buildComponent() {
        return DaggerAppComponent.builder().appModule(new AppModule(this)).build();
    }
}
