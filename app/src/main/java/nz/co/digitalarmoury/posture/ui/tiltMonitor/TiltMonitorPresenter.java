package nz.co.digitalarmoury.posture.ui.tiltMonitor;

import javax.inject.Inject;

import nz.co.digitalarmoury.posture.business.analytics.AnalyticsEventManager;
import nz.co.digitalarmoury.posture.business.bus.Bus;
import nz.co.digitalarmoury.posture.business.bus.BusContext;
import nz.co.digitalarmoury.posture.business.prefs.PrefsRepo;
import nz.co.digitalarmoury.posture.business.sensor.SensorEventsReceiver;
import nz.co.digitalarmoury.posture.business.vibration.VibrationController;
import nz.co.digitalarmoury.posture.ui.Presenter;
import nz.co.digitalarmoury.posture.ui.tiltMonitor.di.PerTiltMonitor;
import rx.subscriptions.CompositeSubscription;

@PerTiltMonitor
public class TiltMonitorPresenter extends Presenter<TiltMonitorView> implements BusContext {
    private final SensorEventsReceiver mSensorEventsReceiver;
    private final VibrationController mVibrationController;
    private final PrefsRepo mPrefsRepo;
    private final AnalyticsEventManager mAnalyticsEventManager;
    private final CompositeSubscription subs;
    private final Bus mBus;

    @Inject
    public TiltMonitorPresenter(Bus bus,
                                SensorEventsReceiver sensorEventsReceiver,
                                VibrationController vibrationController,
                                PrefsRepo prefsRepo,
                                AnalyticsEventManager analyticsEventManager) {
        this.mBus = bus;
        mSensorEventsReceiver = sensorEventsReceiver;
        mVibrationController = vibrationController;
        mPrefsRepo = prefsRepo;
        mAnalyticsEventManager = analyticsEventManager;
        subs = new CompositeSubscription();
    }

    public void setTiltDetection(boolean activated) {
        if (activated) {
            subs.add(mBus.subscribeToBusEvents(this));
            mView.setDialogDismissible(mPrefsRepo.isDialogDismissible());
        }
        else {
            subs.clear();
        }
        mSensorEventsReceiver.setTiltDetection(activated);
        mView.setBackgroundMode(activated);
        mBus.publishServiceStatus(activated);

    }

    /**
     * Bus event
     *
     * @param tiltValid : current tilt was within the correct range
     */
    @Override
    public void tiltValidated(boolean tiltValid) {
        boolean showError = !tiltValid;
        updateNotificationIcon(showError);
        showOverlay(showError);
        showDialog(showError);
        vibrate(showError);
        updateAnalytics(tiltValid);

    }

    private void updateAnalytics(boolean tiltValid) {
        mAnalyticsEventManager.updateAnalytics(tiltValid);
    }

    private void updateNotificationIcon(boolean showError) {
        mView.updateNotification(showError);
    }

    private void showOverlay(boolean showError) {
        if (mPrefsRepo.getIsUsingOverlay()) {
            mView.showAngleIncorrectOverlay(showError, mPrefsRepo.getOverlayPercent());
        }
    }

    private void showDialog(boolean showError) {
        if (mPrefsRepo.getIsUsingDialog()) {
            mView.showAngleIncorrectDialog(showError, mPrefsRepo.isDialogDismissible());
        }
    }

    private void vibrate(boolean showError) {
        if (showError && mPrefsRepo.getIsUsingVibration()) {
            mVibrationController.vibrate();
        }
    }

}