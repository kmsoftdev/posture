package nz.co.digitalarmoury.posture.model.bus;

import nz.co.digitalarmoury.posture.business.bus.BusContext;

/**
 * Created by Think on 04/10/2016.
 */
public abstract class BusEvent {
    public abstract void execute(BusContext busContext);

}
