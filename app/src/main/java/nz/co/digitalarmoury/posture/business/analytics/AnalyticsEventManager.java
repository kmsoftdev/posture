package nz.co.digitalarmoury.posture.business.analytics;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import javax.inject.Inject;
import javax.inject.Singleton;

import nz.co.digitalarmoury.posture.Const;
import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by Think on 12/10/2016.
 */

@Singleton
public class AnalyticsEventManager {

    private final Tracker mAnalyticsTracker;
    private long mTimePromptShowed = -1;

    @Inject
    public AnalyticsEventManager(Tracker tracker) {

        mAnalyticsTracker = tracker;
    }

    public void updateAnalytics(boolean tiltValid) {
        Observable.defer(() -> {
            mAnalyticsTracker.setScreenName(Const.ANGLE_MONITOR);
            if (tiltValid) {

                mAnalyticsTracker.send(new HitBuilders.EventBuilder()
                                               .setCategory("AngleValidation")
                                               .setAction("Angle valid")
                                               .build());
                if (mTimePromptShowed > -1) {

                    long promptStateDuration = System.currentTimeMillis() - mTimePromptShowed;
                    mAnalyticsTracker.send(new HitBuilders.TimingBuilder()
                                                   .setCategory("PromptTiming")
                                                   .setValue(promptStateDuration)
                                                   .setVariable("PromptDisplayedDuration")
                                                   .build());
                    mTimePromptShowed = -1;
                }

            }
            else {
                mAnalyticsTracker.send(new HitBuilders.EventBuilder()
                                               .setCategory("AngleValidation")
                                               .setAction("Angle invalid")
                                               .build());
                if (mTimePromptShowed == -1) {
                    mTimePromptShowed = System.currentTimeMillis();
                }
            }
            return Observable.just(null);
        })
                  .subscribeOn(Schedulers.io())
                  .subscribe(o -> {
                  }, Throwable::printStackTrace);

    }
}
