package nz.co.digitalarmoury.posture.ui.settings.di;

import nz.co.digitalarmoury.posture.ui.settings.SettingsActivity;

import dagger.Module;

@Module
public class SettingsModule {

    private SettingsActivity mInjectee;

    public SettingsModule(SettingsActivity injectee) {
        this.mInjectee = injectee;
    }

}
