package nz.co.digitalarmoury.posture.ui.main.di;

import nz.co.digitalarmoury.posture.ui.main.MainActivity;

import dagger.Subcomponent;

@PerMain
@Subcomponent(modules = MainModule.class)
public interface MainComponent {
    void inject(MainActivity injectee);


}

