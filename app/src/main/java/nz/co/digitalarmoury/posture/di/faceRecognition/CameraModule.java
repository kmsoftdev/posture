package nz.co.digitalarmoury.posture.di.faceRecognition;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Think on 05/10/2016.
 */

@Module
public class CameraModule {

    @Singleton
    @Provides
    Camera provideCamera(Context context) {
        if (deviceHasFrontCamera(context)) {
            int cameraCount = 0;
            Camera cam = null;
            Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
            cameraCount = Camera.getNumberOfCameras();
            for (int camIdx = 0; camIdx < cameraCount; camIdx++) {
                Camera.getCameraInfo(camIdx, cameraInfo);
                if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                    try {
                        cam = Camera.open(camIdx);
                    } catch (RuntimeException e) {
                        Log.e("Camera",
                              "Camera failed to open: " + e.getLocalizedMessage());

                    }
                }
            }
            return cam;
        }
        return null;
    }

    private boolean deviceHasFrontCamera(Context context) {
        if (context.getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA_FRONT)) {
            // this device has front camera
            return true;
        }
        else {
            // no front camera on this device
            return false;
        }
    }

    @Singleton
    @Provides
    SurfaceView provideSurfaceView(Context context) {
        return new SurfaceView(context);
    }

    @Singleton
    @Provides
    SurfaceHolder provideSurfaceHolder(SurfaceView surfaceView) {
        return surfaceView.getHolder();
    }

}
