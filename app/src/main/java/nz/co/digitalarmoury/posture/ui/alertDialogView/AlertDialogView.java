package nz.co.digitalarmoury.posture.ui.alertDialogView;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import nz.co.digitalarmoury.posture.R;
import nz.co.digitalarmoury.posture.databinding.ViewAlertDialogBinding;

public class AlertDialogView extends FrameLayout {
    private boolean mDialogDismissible;
    private ViewAlertDialogBinding mBinding;
    private EventListener mEventListener;

    public AlertDialogView(Context context) {
        super(context);
    }

    public AlertDialogView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AlertDialogView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public AlertDialogView(Context context, boolean strictMode) {
        super(context);
        this.mDialogDismissible = strictMode;
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.view_alert_dialog, this, true);
        mBinding.setEventHandler(this);
        mBinding.setDialogDismissible(mDialogDismissible);
    }

    public boolean isShowing() {
        return this.getParent() != null;
    }

    public void closeSelf() {
        if (mEventListener != null) mEventListener.closeAlertView();
    }

    public void showDetails() {
        if (mEventListener != null) mEventListener.showLearnMore();
    }

    public void setEventListener(EventListener eventListener) {
        this.mEventListener = eventListener;
    }

    public void setDialogDismissible(boolean dialogDismissible) {
        if (mDialogDismissible != dialogDismissible) {
            mDialogDismissible = dialogDismissible;
            if (mBinding != null) mBinding.setDialogDismissible(mDialogDismissible);
        }
    }

    public interface EventListener {
        void closeAlertView();

        void showLearnMore();

    }

}
