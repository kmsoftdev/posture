package nz.co.digitalarmoury.posture.ui.main;

import android.graphics.Bitmap;

import nz.co.digitalarmoury.posture.ui.BaseView;

/**
 * Created by Think on 03/10/2016.
 */

public interface MainView extends BaseView {
    void showMessage(String message);

    void setFrontCameraPreview(Bitmap bitmap);

    void setFaceRecognitionPreview(Bitmap bitmap);

    void markFacesDetected(Integer facesDetected);

    void serviceStatusUpdated(boolean activated);

    void finish();

    void setIsUsingSystemDialog(boolean activated);

    void restoreUserOptions(boolean userOverlayEnabled, boolean systemDialogEnabled, boolean systemDialogStrictMode, boolean useVibrator, boolean userFaceDetectionEnabled);
}
