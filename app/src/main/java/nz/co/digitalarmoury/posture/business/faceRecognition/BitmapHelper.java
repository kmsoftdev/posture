package nz.co.digitalarmoury.posture.business.faceRecognition;

import android.graphics.Bitmap;
import android.graphics.Matrix;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;

/**
 * Created by Think on 11/10/2016.
 */

@Singleton
public class BitmapHelper {

    @Inject
    public BitmapHelper(){

    }

    public static Observable<Bitmap> rotateBitmap(int angle, Bitmap bitmapSrc) {
        return Observable.defer(() -> {
            Matrix matrix = new Matrix();
            matrix.postRotate(angle);
            return Observable.just(Bitmap.createBitmap(bitmapSrc, 0, 0,
                                                       bitmapSrc.getWidth(), bitmapSrc.getHeight(), matrix, true));
        });

    }
}
