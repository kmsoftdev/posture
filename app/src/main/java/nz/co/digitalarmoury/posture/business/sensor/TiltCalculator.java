package nz.co.digitalarmoury.posture.business.sensor;

import android.content.Context;
import android.view.Surface;
import android.view.WindowManager;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;

@Singleton
public class TiltCalculator {

    private final Context mContext;
    private final WindowManager mWindowManager;

    @Inject
    public TiltCalculator(Context context, WindowManager windowManager) {

        mContext = context;
        mWindowManager = windowManager;
    }

    public Observable<Float> getCurrentTilt(float[] mOrientationAngles) {
        return Observable.defer(() -> {
            switch (getRotation(mContext)) {
                case "portrait":
                    return Observable.just(-mOrientationAngles[1] * 57.2957795f);
                case "reverse portrait":
                    return Observable.just(mOrientationAngles[1] * 57.2957795f);
                case "landscape":
                    return Observable.just(-mOrientationAngles[2] * 57.2957795f);

                case "reverse landscape":
                    return Observable.just(mOrientationAngles[2] * 57.2957795f);
            }
            return Observable.just(0f);
        });
    }

    public String getRotation(Context context) {
        final int rotation = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getOrientation();
        switch (rotation) {
            case Surface.ROTATION_0:
                return "portrait";
            case Surface.ROTATION_90:
                return "landscape";
            case Surface.ROTATION_180:
                return "reverse portrait";
            default:
                return "reverse landscape";
        }
    }
}
