package nz.co.digitalarmoury.posture.business.sensor;

import android.util.Log;

import javax.inject.Inject;
import javax.inject.Singleton;

import nz.co.digitalarmoury.posture.business.faceRecognition.UserFaceRecognizer;
import nz.co.digitalarmoury.posture.business.prefs.PrefsRepo;
import rx.Observable;
import rx.schedulers.Schedulers;

@Singleton
public class TiltValidator {

    private final PrefsRepo mPrefsRepo;
    private final UserFaceRecognizer mUserFaceRecognizer;

    @Inject
    public TiltValidator(PrefsRepo prefsRepo, UserFaceRecognizer userFaceRecognizer) {
        mPrefsRepo = prefsRepo;
        this.mUserFaceRecognizer = userFaceRecognizer;
    }

    public Observable<Boolean> validateAngle(float tilt) {
        return checkTiltAngle(tilt)
                .flatMap(angleCorrect -> {
                    if (angleCorrect) {
                        return Observable.just(true);
                    }
                    else {

                        return mPrefsRepo.getIsUsingFaceRecognition()
                                ? mUserFaceRecognizer.userLookingAtDevice()
                                                     .map(userLookingAtDevice -> !userLookingAtDevice)
                                : Observable.just(angleCorrect);
                    }
                })
                .subscribeOn(Schedulers.computation());
    }

    private Observable<Boolean> checkTiltAngle(float tilt) {
        return Observable.defer(() -> {
            Log.d("kmtag", "tilt: " + tilt);
            float tiltFlat = mPrefsRepo.getAngleFlat();
            return tilt < tiltFlat ? Observable.just(true)
                    : Observable.just(tilt > mPrefsRepo.getAngleLow() && tilt < mPrefsRepo.getAngleHigh());
        });
    }

}
