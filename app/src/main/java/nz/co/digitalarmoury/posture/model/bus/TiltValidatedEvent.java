package nz.co.digitalarmoury.posture.model.bus;

import nz.co.digitalarmoury.posture.business.bus.BusContext;

/**
 * Created by Think on 04/10/2016.
 */
public class TiltValidatedEvent extends BusEvent {
    private final boolean mValid;

    public TiltValidatedEvent(boolean valid) {
        super();
        this.mValid = valid;
    }
    public void execute(BusContext busContext){
        busContext.tiltValidated(mValid);
    }
}
