package nz.co.digitalarmoury.posture.di;

import android.app.KeyguardManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.WindowManager;

import nz.co.digitalarmoury.posture.App;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private static final String KEY_SHARED_PREFS = "userPreferences";
    private App mApp;

    public AppModule(App mApp) {
        this.mApp = mApp;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return this.mApp;
    }

    @Provides
    @Singleton
    WindowManager provideWindowManager(Context context) {
        return (WindowManager) context.getSystemService(context.WINDOW_SERVICE);
    }

    @Provides
    @Singleton
    SharedPreferences provideSharedPrefs(Context context) {
        return context.getSharedPreferences(KEY_SHARED_PREFS, Context.MODE_PRIVATE);
    }

    @Provides
    @Singleton
    KeyguardManager provideKeyguardManager(Context context) {
        return (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
    }

}

