package nz.co.digitalarmoury.posture.model.bus;

import nz.co.digitalarmoury.posture.business.bus.ServiceStatusListener;

/**
 * Created by Think on 09/10/2016.
 */
public class ServiceStatusUpdatedEvent {
    private final boolean mActivated;

    public ServiceStatusUpdatedEvent(boolean activated) {
        this.mActivated = activated;
    }

    public void execute(ServiceStatusListener serviceStatusListener) {
        serviceStatusListener.serviceStatusUpdated(mActivated);
    }

    public boolean getServiceStatus() {
        return this.mActivated;
    }
}
