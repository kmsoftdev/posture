package nz.co.digitalarmoury.posture.business.sensor;

import nz.co.digitalarmoury.posture.model.sensors.SensorEventsWrapper;
import rx.Observable;
import rx.schedulers.Schedulers;

public class AccelEventsHandler implements SensorEventsHandler {

    private final TiltCalculator mTiltCalculator;
    private final TiltValidator mTiltValidator;
    private float[] floats = new float[3];

    public AccelEventsHandler(TiltCalculator tiltCalculator, TiltValidator tiltValidator) {

        mTiltCalculator = tiltCalculator;
        mTiltValidator = tiltValidator;
    }

    @Override
    public Observable<Boolean> validateCurrentTilt(SensorEventsWrapper sensorEventsWrapper) {
        return getCurrentAngle(sensorEventsWrapper)
                .flatMap(this::validateAngle)
                .subscribeOn(Schedulers.computation());
    }

    private Observable<Float> getCurrentAngle(SensorEventsWrapper sensorEventsWrapper) {
        return Observable.defer(() -> {
            
            floats = sensorEventsWrapper.getAccelerometerSensorEvent().values.clone();

            float norm_Of_g = (float) Math.sqrt(floats[0] * floats[0] + floats[1] * floats[1] + floats[2] * floats[2]);

// Normalize the accelerometer vector
            floats[0] = floats[0] / norm_Of_g;
            floats[1] = floats[1] / norm_Of_g;
            floats[2] = floats[2] / norm_Of_g;
            float inclination = (float) Math.toDegrees(Math.acos(floats[2]));
            return Observable.just(inclination);
        });
    }
    private Observable<Boolean> validateAngle(float currentAngle){
        return mTiltValidator.validateAngle(currentAngle);
    }
}
