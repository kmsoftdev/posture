package nz.co.digitalarmoury.posture.ui.settings;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Toast;

import javax.inject.Inject;
import javax.inject.Named;

import nz.co.digitalarmoury.posture.App;
import nz.co.digitalarmoury.posture.R;
import nz.co.digitalarmoury.posture.databinding.ActivitySettingsBinding;
import nz.co.digitalarmoury.posture.ui.BaseActivity;
import nz.co.digitalarmoury.posture.ui.cameraOptions.CameraOptionsActivity;
import nz.co.digitalarmoury.posture.ui.main.MainActivity;
import nz.co.digitalarmoury.posture.ui.settings.di.SettingsModule;
import nz.co.digitalarmoury.posture.ui.tiltMonitor.TiltMonitorService;

public class SettingsActivity extends BaseActivity<SettingsPresenter, ActivitySettingsBinding, SettingsView> implements SettingsView {
    public static final int REQ_PERMISSION_LOCATION = 379;
    public static final int REQ_PERMISSION_VIBRATOR = 380;
    public static final int REQ_PERMISSION_FACE_DETECTION_OPTIONS = 382;
    private static final int REQ_FACE_RECOGNITION = 381;
    @Inject @Named("hasMagnetometer") boolean deviceHasMagnetometer;

    public static Intent createIntent(Context context) {
        Intent intent = new Intent(context, SettingsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }

    @Override
    protected void customOnCreate(Bundle savedInstanceState) {
        super.customOnCreate(savedInstanceState);
        mBinding.setEventListener(this);
    }

    @Override
    public void serviceStatusUpdated(boolean activated) {
        mBinding.setServiceActivated(activated);
    }

    @Override
    public void restoreUserSettings(boolean isUsingOverlay,
                                    boolean isUsingDialog,
                                    boolean dialogDismissible,
                                    boolean isUsingVibration,
                                    boolean isUsingFaceRecognition) {

        mBinding.overlaySwitch.setChecked(isUsingOverlay);
        mBinding.dialogSwitch.setChecked(isUsingDialog);
        mBinding.setUseDialogActivated(isUsingDialog);
        mBinding.dialogDismissibleSwitch.setChecked(dialogDismissible);
        mBinding.vibrationSwitch.setChecked(isUsingVibration);
        mBinding.faceRecognitionSwitch.setChecked(isUsingFaceRecognition);
    }

    @Override
    protected void injectDependencies() {
        ((App) getApplication()).getAppComponent().sub(new SettingsModule(this)).inject(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_settings;
    }

    @Override
    protected SettingsView getPresenterView() {
        return this;
    }

    public void startService(boolean start) {
        Log.d("kmtag", "hasMagnetometer: " + deviceHasMagnetometer);
        if (!deviceHasMagnetometer) {
            startService(TiltMonitorService.getSetActivatedIntent(this, start));
            finish();
        }
        else {
            if (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
                startService(TiltMonitorService.getSetActivatedIntent(this, start));
                finish();
            }
            else {
                requestPermission(Manifest.permission.ACCESS_FINE_LOCATION, REQ_PERMISSION_LOCATION);
            }
        }
    }

    private void requestPermission(String permission, int requestCode) {

        if (requestCode != -1)
            ActivityCompat.requestPermissions(this, new String[]{permission}, requestCode);
    }

    private boolean hasPermission(String permission) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && permission != null) {
            if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {

                return false;
            }

        }
        return true;
    }

    public void showFeatureInfo(View view) {
        switch (view.getId()) {
            case R.id.overlayInfo:
                Toast.makeText(this, getString(R.string.overlay_info), Toast.LENGTH_LONG).show();
                break;
            case R.id.dialogInfo:
                Toast.makeText(this, getString(R.string.dialog_info), Toast.LENGTH_LONG).show();
                break;
            case R.id.dialogDismissibleInfo:
                Toast.makeText(this, getString(R.string.strict_mode_info), Toast.LENGTH_LONG).show();
                break;
            case R.id.vibrationInfo:
                Toast.makeText(this, getString(R.string.vibration_info), Toast.LENGTH_LONG).show();
                break;
            case R.id.faceRecognitionInfo:
                Toast.makeText(this, getString(R.string.face_recognition_info), Toast.LENGTH_LONG).show();
                break;

        }
    }

    public void optionToggled(CompoundButton optionSwitch, boolean checked) {
        switch (optionSwitch.getId()) {
            case R.id.overlaySwitch:
                mPresenter.setOverlay(checked);

                break;
            case R.id.dialogSwitch:
                mPresenter.setDialog(checked);
                mBinding.setUseDialogActivated(checked);
                break;
            case R.id.dialogDismissibleSwitch:
                mPresenter.setStrictMode(checked);
                break;
            case R.id.vibrationSwitch:
                if (hasPermission(Manifest.permission.VIBRATE)) {
                    mPresenter.setVibration(checked);
                }
                else {
                    requestPermission(Manifest.permission.VIBRATE, REQ_PERMISSION_VIBRATOR);
                }
                break;
            case R.id.faceRecognitionSwitch:
                if (hasPermission(Manifest.permission.CAMERA)) {
                    mPresenter.setFaceRecognition(checked);
                }
                else {
                    requestPermission(Manifest.permission.CAMERA, REQ_FACE_RECOGNITION);
                }
                break;
        }
    }

    public void showOptions(View view) {
        switch (view.getId()) {
            case R.id.faceRecognitionOptions:
                if (hasPermission(Manifest.permission.CAMERA)) {
                    startActivity(CameraOptionsActivity.createIntent(this));
                }
                else
                    requestPermission(Manifest.permission.CAMERA, REQ_PERMISSION_FACE_DETECTION_OPTIONS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQ_PERMISSION_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startService(true);
                }
                else {
                    Toast.makeText(this, "This permission is required in order to determine you device's position," +
                                           " data obtained is neither collected nor stored. ",
                                   Toast.LENGTH_SHORT).show();
                }
                return;
            case REQ_PERMISSION_VIBRATOR:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mPresenter.setVibration(true);
                }
                else {
                    Toast.makeText(this, "Permission denied, vibration functionality is now disabled. ", Toast.LENGTH_SHORT).show();
                    mBinding.setEventListener(null);
                    mBinding.vibrationSwitch.setChecked(false);
                    mBinding.setEventListener(this);
                }
                return;
            case REQ_FACE_RECOGNITION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mPresenter.setFaceRecognition(true);
                }
                else {
                    Toast.makeText(this, "This functionality requires access to device's camera", Toast.LENGTH_SHORT).show();
                    mBinding.setEventListener(null);
                    mBinding.faceRecognitionSwitch.setChecked(false);
                    mBinding.setEventListener(this);

                }
                return;
            case REQ_PERMISSION_FACE_DETECTION_OPTIONS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startActivity(CameraOptionsActivity.createIntent(this));
                }
                else {
                    Toast.makeText(this, "This functionality requires access to device's camera", Toast.LENGTH_SHORT).show();
                }
        }
    }

    public void showLegacyControls(View view) {
        startActivity(MainActivity.createIntent(this));
    }
}