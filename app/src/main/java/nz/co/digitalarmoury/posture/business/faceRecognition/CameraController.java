package nz.co.digitalarmoury.posture.business.faceRecognition;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;

import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Singleton;

import nz.co.digitalarmoury.posture.business.prefs.PrefsRepo;
import nz.co.digitalarmoury.posture.model.camera.ByteArrayHolder;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

@Singleton
public class CameraController {
    private final Context mContext;
    private final PrefsRepo mPrefsRepo;
    private final WindowManager mWindowManager;
    private Camera mCamera;

    @Inject
    public CameraController(Context context,
                            PrefsRepo prefsRepo,
                            WindowManager windowManager) {

        mContext = context;
        mPrefsRepo = prefsRepo;
        mWindowManager = windowManager;
    }

    public Observable<Bitmap> takePicture() {
        return hasFrontCamera(mContext)
//                .filter(Boolean::booleanValue)
.flatMap(deviceHasFrontCamera -> openFrontCamera())
.map(this::adjustPictureSize)
.flatMap(this::getBitmapByteArray)
.flatMap(this::getBitmap)
.flatMap(this::adjustPictureRotation);

    }

    private Observable<Boolean> hasFrontCamera(Context context) {
        return Observable.defer(() -> {
            if (context.getPackageManager().hasSystemFeature(
                    PackageManager.FEATURE_CAMERA_FRONT)) {
                // this device has front camera
                return Observable.just(true);
            }
            else {
                // no front camera on this device
                return Observable.just(false);
            }
        });
    }

    private Observable<Camera> openFrontCamera() {
        return Observable.defer(() -> {

            if (mCamera != null) {
                try {
                    mCamera.stopPreview();
                    mCamera.release();
                } catch (RuntimeException e) {
                    //was already released
                }
            }

            int cameraCount = 0;

            Camera cam = null;
            Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
            cameraCount = Camera.getNumberOfCameras();
            for (int camIdx = 0; camIdx < cameraCount; camIdx++) {
                Camera.getCameraInfo(camIdx, cameraInfo);
                if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                    try {
                        cam = Camera.open(camIdx);
                    } catch (RuntimeException e) {
                        Log.e("Camera",
                              "Camera failed to open: " + e.getLocalizedMessage());
                    }
                }
            }
            if (cam != null) {
                mCamera = cam;
                return Observable.just(cam);
            }
            else {
                return Observable.error(new Throwable("Failed to initialize camera"));
            }
        });

    }

    private Camera adjustPictureSize(Camera camera) {

        Camera.Parameters params = camera.getParameters();
        int amountOfPreviews = params.getSupportedPreviewSizes().size();
        Camera.Size previewSize = params.getSupportedPreviewSizes().get(amountOfPreviews > 2 ? (int) amountOfPreviews / 2 : 0);
        int amountOfPictureSizes = params.getSupportedPictureSizes().size();
        Camera.Size pictureSize = params.getSupportedPictureSizes().get(amountOfPictureSizes > 2 ? (int) amountOfPictureSizes / 2 : 0);
        params.setPreviewSize(previewSize.width, previewSize.height);
        params.setPictureSize(pictureSize.width, pictureSize.height);
        camera.setParameters(params);

        return camera;
    }

    private Observable<ByteArrayHolder> getBitmapByteArray(Camera camera) {
        return Observable.create(subscriber -> {
            SurfaceView surfaceView = new SurfaceView(mContext);

            WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.TYPE_TOAST,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    PixelFormat.TRANSPARENT);
            layoutParams.gravity = Gravity.LEFT | Gravity.TOP;
            layoutParams.width = 1;
            layoutParams.height = 1;

            mWindowManager.addView(surfaceView, layoutParams);
            SurfaceHolder surfaceHolder = surfaceView.getHolder();
            surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

            surfaceHolder.addCallback(new SurfaceHolder.Callback() {
                @Override
                public void surfaceCreated(SurfaceHolder surfaceHolder) {

                }

                @Override
                public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
                    try {
                        camera.setPreviewDisplay(surfaceHolder);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    camera.startPreview();
                    try {
                        //this is so fucking stupid, can we do something about it???
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    camera.takePicture(null, null, (bytes, camera1) -> {
                        subscriber.onNext(new ByteArrayHolder(bytes));
                        camera1.stopPreview();
                        camera1.release();
                        mWindowManager.removeView(surfaceView);
                        subscriber.onCompleted();
                    });
                }

                @Override
                public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

                }
            });
        }).subscribeOn(AndroidSchedulers.mainThread()).cast(ByteArrayHolder.class);
    }

    private Observable<Bitmap> getBitmap(ByteArrayHolder byteArrayHolder) {
        return Observable.defer(() -> {
            Bitmap bitmap = decodeBitmap(byteArrayHolder.getBitmapData());
            return Observable.just(bitmap);
        }).subscribeOn(Schedulers.computation());
    }

    private Bitmap decodeBitmap(byte[] data) {
        return BitmapFactory.decodeByteArray(data, 0, data.length);
    }

    private Observable<Bitmap> adjustPictureRotation(Bitmap bitmap) {
        return Observable.defer(() -> {
            Matrix matrix = new Matrix();
            matrix.postRotate(mPrefsRepo.getCameraRotationCorrection() + getScreenRotation());

            return Observable.just(Bitmap.createBitmap(bitmap, 0, 0,
                                                       bitmap.getWidth(), bitmap.getHeight(), matrix, true));
        });
    }

    private int getScreenRotation() {
        Display display = mWindowManager.getDefaultDisplay();
        switch (display.getRotation()) {
            case 0:
                return 0;
            case 1:
                return 90;
            case 2:
                return 180;
            case 3:
                return 270;
            default:
                return 0;

        }

    }

}
