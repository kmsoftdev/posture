package nz.co.digitalarmoury.posture.ui.learnMore;

import nz.co.digitalarmoury.posture.ui.Presenter;
import nz.co.digitalarmoury.posture.ui.learnMore.di.PerLearnMore;

import javax.inject.Inject;

@PerLearnMore
public class LearnMorePresenter extends Presenter<LearnMoreView> {
    @Inject
    public LearnMorePresenter() {
    }

}
