package nz.co.digitalarmoury.posture.ui.tiltMonitor.di;

import nz.co.digitalarmoury.posture.ui.tiltMonitor.TiltMonitorService;

import dagger.Subcomponent;

@PerTiltMonitor
@Subcomponent(modules = TiltMonitorModule.class)
public interface TiltMonitorComponent {
    void inject(TiltMonitorService injectee);
}

