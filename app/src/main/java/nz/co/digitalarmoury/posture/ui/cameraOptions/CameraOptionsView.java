package nz.co.digitalarmoury.posture.ui.cameraOptions;

import android.graphics.Bitmap;

import nz.co.digitalarmoury.posture.ui.BaseView;

/**
 * Created by Think on 11/10/2016.
 */

public interface CameraOptionsView extends BaseView {

    void setFrontCameraPreview(Bitmap bitmap);

    void showMessage(String message);

    void userFaceDetected(boolean detected);

    void highlightTakePictureButton();

}
