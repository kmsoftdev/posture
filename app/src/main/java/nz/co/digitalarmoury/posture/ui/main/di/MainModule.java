package nz.co.digitalarmoury.posture.ui.main.di;

import nz.co.digitalarmoury.posture.ui.main.MainActivity;

import dagger.Module;

@Module
public class MainModule {

    private MainActivity mInjectee;

    public MainModule(MainActivity injectee) {
        this.mInjectee = injectee;
    }

}
