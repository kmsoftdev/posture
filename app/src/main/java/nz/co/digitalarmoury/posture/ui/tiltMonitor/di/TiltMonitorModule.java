package nz.co.digitalarmoury.posture.ui.tiltMonitor.di;

import nz.co.digitalarmoury.posture.ui.tiltMonitor.TiltMonitorService;

import dagger.Module;

@Module
public class TiltMonitorModule {

    private TiltMonitorService mInjectee;

    public TiltMonitorModule(TiltMonitorService injectee) {
        this.mInjectee = injectee;
    }

}
