package nz.co.digitalarmoury.posture.di.analytics;

import android.content.Context;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import nz.co.digitalarmoury.posture.R;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


@Module
public class AnalyticsModule {

    @Singleton
    @Provides
    Tracker provideTracker(Context context) {
        return GoogleAnalytics.getInstance(context).newTracker(R.xml.global_tracker);
    }
}
