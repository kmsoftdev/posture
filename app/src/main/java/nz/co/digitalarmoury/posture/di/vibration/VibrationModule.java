package nz.co.digitalarmoury.posture.di.vibration;

import android.content.Context;
import android.os.Vibrator;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class VibrationModule {

    @Singleton
    @Provides
    Vibrator provideVibrator(Context context) {
        return (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
    }
}
