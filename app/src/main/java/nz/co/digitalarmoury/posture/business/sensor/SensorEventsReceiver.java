package nz.co.digitalarmoury.posture.business.sensor;

/**
 * Created by Think on 13/10/2016.
 */

public interface SensorEventsReceiver {
    void setTiltDetection(boolean activated);
}
