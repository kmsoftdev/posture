package nz.co.digitalarmoury.posture.ui.cameraOptions;

import android.graphics.Bitmap;

import nz.co.digitalarmoury.posture.business.faceRecognition.BitmapHelper;
import nz.co.digitalarmoury.posture.business.faceRecognition.UserFaceRecognizer;
import nz.co.digitalarmoury.posture.business.prefs.PrefsRepo;
import nz.co.digitalarmoury.posture.ui.Presenter;
import nz.co.digitalarmoury.posture.ui.cameraOptions.di.PerCameraOptions;

import javax.inject.Inject;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

@PerCameraOptions
public class CameraOptionsPresenter extends Presenter<CameraOptionsView> {
    private final UserFaceRecognizer mUserFaceRecognizer;
    private final PrefsRepo mPrefsRepo;
    private Bitmap mBitmapCache;

    @Inject
    public CameraOptionsPresenter(UserFaceRecognizer userFaceRecognizer,
                                  PrefsRepo prefsRepo) {
        mUserFaceRecognizer = userFaceRecognizer;
        mPrefsRepo = prefsRepo;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    public void takePicture() {
        mUserFaceRecognizer.getCurrentFrontCameraImage()
                           .subscribeOn(Schedulers.computation())
                           .observeOn(AndroidSchedulers.mainThread())
                           .subscribe(bitmap -> {
                               mBitmapCache = bitmap;
                               mView.setFrontCameraPreview(bitmap);
                               mUserFaceRecognizer.getNumberOfFacesDetected(bitmap)
                                                  .subscribeOn(Schedulers.computation())
                                                  .observeOn(AndroidSchedulers.mainThread())
                                                  .subscribe(facesDetected -> {
                                                      mView.userFaceDetected(facesDetected > 0);
                                                  }, throwable -> {
                                                      mView.showMessage(throwable.getMessage());
                                                  });
                           }, throwable -> {
                               mView.showMessage(throwable.getLocalizedMessage());
                           });

    }

    public void rotateBitmap(int rotationAngle) {
        if (mBitmapCache != null || !mBitmapCache.isRecycled()) {
            BitmapHelper.rotateBitmap(rotationAngle, mBitmapCache)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(rotatedBitmap -> {
                            mBitmapCache = rotatedBitmap;
                            mPrefsRepo.setCameraRotationCorrection(rotationAngle);
                            mView.setFrontCameraPreview(rotatedBitmap);
                        }, throwable -> {
                            mView.showMessage(throwable.getMessage());
                        });

        }
        else {
            mView.showMessage("Take a picture first!");
            mView.highlightTakePictureButton();
        }
    }

}
