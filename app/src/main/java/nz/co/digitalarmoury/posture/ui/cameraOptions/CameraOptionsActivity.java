package nz.co.digitalarmoury.posture.ui.cameraOptions;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.view.MenuItem;
import android.widget.Toast;

import nz.co.digitalarmoury.posture.App;
import nz.co.digitalarmoury.posture.R;
import nz.co.digitalarmoury.posture.databinding.ActivityCameraOptionsBinding;
import nz.co.digitalarmoury.posture.ui.cameraOptions.di.CameraOptionsModule;
import nz.co.digitalarmoury.posture.ui.BaseActivity;

public class CameraOptionsActivity extends BaseActivity<CameraOptionsPresenter,
        ActivityCameraOptionsBinding, CameraOptionsView> implements CameraOptionsView {

    public static Intent createIntent(Context context) {
        Intent intent = new Intent(context, CameraOptionsActivity.class);
        return intent;
    }

    @Override
    protected void customOnCreateView() {
        super.customOnCreateView();
        mBinding.setEventListener(mPresenter);
        setSupportActionBar(mBinding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public void setFrontCameraPreview(Bitmap bitmap) {
        mBinding.frontCameraPreview.setImageBitmap(bitmap);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void userFaceDetected(boolean detected) {
        mBinding.frontCameraPreview.setColorFilter(
                detected ?
                        new PorterDuffColorFilter(Color.parseColor("#785FBC34"), PorterDuff.Mode.SRC_ATOP)
                        : null);
    }

    @Override
    public void highlightTakePictureButton() {
        mBinding.takePicture.animate().scaleX(1.2f).scaleY(1.2f)
                            .withEndAction(new Runnable() {
                                @Override
                                public void run() {
                                    mBinding.takePicture.animate().scaleX(1).scaleY(1);
                                }
                            });
    }

    @Override
    protected void injectDependencies() {
        ((App) getApplication()).getAppComponent().sub(new CameraOptionsModule(this)).inject(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_camera_options;
    }

    @Override
    protected CameraOptionsView getPresenterView() {
        return this;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
