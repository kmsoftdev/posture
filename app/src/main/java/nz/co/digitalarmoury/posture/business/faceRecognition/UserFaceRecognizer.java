package nz.co.digitalarmoury.posture.business.faceRecognition;

import android.graphics.Bitmap;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;

@Singleton
public class UserFaceRecognizer {

    private final CameraController mCameraController;
    private final FaceDetectorController mFaceDetectorController;

    @Inject
    public UserFaceRecognizer(CameraController cameraController, FaceDetectorController faceDetectorController) {

        mCameraController = cameraController;
        mFaceDetectorController = faceDetectorController;
    }

    public Observable<Boolean> userLookingAtDevice() {
        return getCurrentFrontCameraImage()
                .flatMap(this::getNumberOfFacesDetected)
                .map(numberOfFacesDetected -> numberOfFacesDetected > 0);
    }

    public Observable<Bitmap> getCurrentFrontCameraImage() {
        return mCameraController.takePicture();
    }

    public Observable<Integer> getNumberOfFacesDetected(Bitmap bitmap) {
        return mFaceDetectorController.facesDetected(bitmap);
    }

}
