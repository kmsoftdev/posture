package nz.co.digitalarmoury.posture.ui.main;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import nz.co.digitalarmoury.posture.BR;
import nz.co.digitalarmoury.posture.business.prefs.PrefsRepo;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class MainUiState extends BaseObservable {

    private final PrefsRepo mPrefsRepo;

    @Inject
    public MainUiState(PrefsRepo prefsRepo) {
        mPrefsRepo = prefsRepo;
    }

    @Bindable
    public boolean getIsUsingOverlay() {
        return mPrefsRepo.getIsUsingOverlay();
    }

    public void setIsUsingOverlay(boolean isUsingOverlay) {
        mPrefsRepo.setIsUsingOverlay(isUsingOverlay);
        notifyPropertyChanged(BR.isUsingOverlay);
    }

}
