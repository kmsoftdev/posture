package nz.co.digitalarmoury.posture.business.bus;

import com.jakewharton.rxrelay.BehaviorRelay;
import com.jakewharton.rxrelay.PublishRelay;
import nz.co.digitalarmoury.posture.model.bus.BusEvent;
import nz.co.digitalarmoury.posture.model.bus.ServiceStatusUpdatedEvent;
import nz.co.digitalarmoury.posture.model.bus.TiltValidatedEvent;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

@Singleton
public class Bus {
    private final PublishRelay<BusEvent> eventPublisher;
    private final BehaviorRelay<ServiceStatusUpdatedEvent> serviceStatusPublisher;

    @Inject
    public Bus() {
        eventPublisher = PublishRelay.create();
        serviceStatusPublisher = BehaviorRelay.create();
    }

    public Subscription subscribeToBusEvents(BusContext busContext) {
        return eventPublisher
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(busEvent -> {
                    busEvent.execute(busContext);
                });
    }

    public void publishTiltValidatedEvent(boolean valid) {
        this.eventPublisher.call(new TiltValidatedEvent(valid));
    }

    public Subscription subscribeToServiceStatusUpdates(ServiceStatusListener serviceStatusListener) {
        return serviceStatusPublisher.subscribeOn(AndroidSchedulers.mainThread())
                                     .subscribe(serviceStatusUpdatedEvent -> {
                                         serviceStatusListener.serviceStatusUpdated(serviceStatusUpdatedEvent.getServiceStatus());
                                     });
    }

    public void publishServiceStatus(boolean activated) {
        this.serviceStatusPublisher.call(new ServiceStatusUpdatedEvent(activated));
    }
}
