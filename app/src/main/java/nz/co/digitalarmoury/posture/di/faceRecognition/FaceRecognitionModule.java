package nz.co.digitalarmoury.posture.di.faceRecognition;

import dagger.Module;

/**
 * Created by Think on 05/10/2016.
 */

@Module(includes = {CameraModule.class, FaceDetectorModule.class})
public class FaceRecognitionModule {
}
