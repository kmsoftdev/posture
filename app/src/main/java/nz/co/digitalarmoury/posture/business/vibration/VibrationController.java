package nz.co.digitalarmoury.posture.business.vibration;

import android.os.Vibrator;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Think on 09/10/2016.
 */

@Singleton
public class VibrationController {

    private final Vibrator mVibrator;

    @Inject
    public VibrationController(Vibrator vibrator) {

        mVibrator = vibrator;
    }

    public void vibrate() {
        mVibrator.vibrate(30);
    }

}
