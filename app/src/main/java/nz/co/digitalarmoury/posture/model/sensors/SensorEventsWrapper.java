package nz.co.digitalarmoury.posture.model.sensors;

import android.hardware.SensorEvent;

public class SensorEventsWrapper {
    private final SensorEvent accelerometerSensorEvent;
    private final SensorEvent magnetometerSensorEvent;

    public SensorEventsWrapper(SensorEvent accelerometerSensorEvent, SensorEvent magnetometerSensorEvent) {

        this.accelerometerSensorEvent = accelerometerSensorEvent;
        this.magnetometerSensorEvent = magnetometerSensorEvent;
    }

    public SensorEventsWrapper(SensorEvent accelerometerSensorEvent) {
        this.accelerometerSensorEvent = accelerometerSensorEvent;
        magnetometerSensorEvent = null;
    }

    public SensorEvent getAccelerometerSensorEvent() {
        return accelerometerSensorEvent;
    }

    public SensorEvent getMagnetometerSensorEvent() {
        return magnetometerSensorEvent;
    }
}
