package nz.co.digitalarmoury.posture.business.bus;

/**
 * Created by Think on 09/10/2016.
 */
public interface ServiceStatusListener {
    void serviceStatusUpdated(boolean mActivated);

}
