package nz.co.digitalarmoury.posture.business.sensor;

import nz.co.digitalarmoury.posture.model.sensors.SensorEventsWrapper;
import rx.Observable;

/**
 * Created by Think on 13/10/2016.
 */

public interface SensorEventsHandler {
    Observable<Boolean> validateCurrentTilt(SensorEventsWrapper sensorEventsWrapper);
}
