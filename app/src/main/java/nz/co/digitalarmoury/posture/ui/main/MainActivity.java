package nz.co.digitalarmoury.posture.ui.main;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import nz.co.digitalarmoury.posture.App;
import nz.co.digitalarmoury.posture.R;
import nz.co.digitalarmoury.posture.databinding.ActivityMainBinding;
import nz.co.digitalarmoury.posture.ui.main.di.MainModule;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity implements MainView {
    public static final int REQUEST_ALERT_WINDOW_PERMISSION = 1;
    private static final int REQUEST_PERMISSIONS = 2;
    String[] requiredPermissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CAMERA};
    @Inject MainPresenter mPresenter;
    private ActivityMainBinding mBinding;

    public static Intent createIntent(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App) getApplication()).getAppComponent().sub(new MainModule(this)).inject(this);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mPresenter.setView(this);
        mPresenter.onCreateView();
        setSupportActionBar(mBinding.toolbar);
        mBinding.setEventHandler(mPresenter);
        checkPermissions();
    }

    @Override
    public void restoreUserOptions(boolean userOverlayEnabled,
                                   boolean systemDialogEnabled,
                                   boolean systemDialogStrictMode,
                                   boolean useVibrator,
                                   boolean userFaceDetectionEnabled) {
        mBinding.content.userOverlay.setChecked(userOverlayEnabled);
        mBinding.content.showSystemDialog.setChecked(systemDialogEnabled);
        mBinding.setIsUsingSystemDialog(systemDialogEnabled);
        mBinding.content.dialogStrictMode.setChecked(systemDialogStrictMode);
        mBinding.content.useVibrator.setChecked(useVibrator);
        mBinding.content.userFaceDetectionCheck.setChecked(userFaceDetectionEnabled);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPresenter.onPause();
    }

    private void checkPermissions() {
       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(this)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                           Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, REQUEST_ALERT_WINDOW_PERMISSION);
            }
        }*/
        if (!hasPermissions(this, Manifest.permission.ACCESS_FINE_LOCATION)) {

            ActivityCompat.requestPermissions(this,
                                              requiredPermissions,
                                              REQUEST_PERMISSIONS);
        }

    }

    private boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroyView();
    }

    @Override
    public void setIsUsingSystemDialog(boolean activated) {
        mBinding.setIsUsingSystemDialog(activated);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ALERT_WINDOW_PERMISSION) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!Settings.canDrawOverlays(this)) {
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSIONS) {
            if (grantResults.length > 0) {
                //TODO modify tilt detection method
            }
            else {
                //TODO modify tilt detection method
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setFrontCameraPreview(Bitmap bitmap) {
        mBinding.content.frontCameraPreview.setImageDrawable(new BitmapDrawable(getResources(), bitmap));
    }

    @Override
    public void setFaceRecognitionPreview(Bitmap bitmap) {
        mBinding.content.testFaceRecognitionPreview.setImageDrawable(new BitmapDrawable(getResources(), bitmap));

    }

    @Override
    public void markFacesDetected(Integer facesDetected) {
        mBinding.content.testFaceRecognitionPreview.setColorFilter(
                facesDetected > 0
                        ? new PorterDuffColorFilter(Color.parseColor("#785FBC34"), PorterDuff.Mode.SRC_ATOP)
                        : null);

    }

    @Override
    public void serviceStatusUpdated(boolean activated) {
        mBinding.setServiceActivated(activated);
    }

}

