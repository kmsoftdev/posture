package nz.co.digitalarmoury.posture.ui.learnMore;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import nz.co.digitalarmoury.posture.App;
import nz.co.digitalarmoury.posture.R;
import nz.co.digitalarmoury.posture.databinding.ActivityLearnMoreBinding;
import nz.co.digitalarmoury.posture.ui.learnMore.di.LearnMoreModule;
import nz.co.digitalarmoury.posture.ui.BaseActivity;

public class LearnMoreActivity extends BaseActivity<LearnMorePresenter, ActivityLearnMoreBinding, LearnMoreView>
        implements LearnMoreView {

    public static Intent createIntent(Context context) {
        Intent intent = new Intent(context, LearnMoreActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }

    @Override
    protected void injectDependencies() {
        ((App) getApplication()).getAppComponent().sub(new LearnMoreModule(this)).inject(this);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(mBinding.toolbar);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_learn_more;
    }

    @Override
    protected LearnMoreView getPresenterView() {
        return this;
    }

}
