package nz.co.digitalarmoury.posture.business.bus;

/**
 * Created by Think on 04/10/2016.
 */
public interface BusContext {

    void tiltValidated(boolean valid);
}
