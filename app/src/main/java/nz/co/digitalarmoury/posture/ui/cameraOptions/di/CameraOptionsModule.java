package nz.co.digitalarmoury.posture.ui.cameraOptions.di;

import nz.co.digitalarmoury.posture.ui.cameraOptions.CameraOptionsActivity;

import dagger.Module;

@Module
public class CameraOptionsModule {

    private CameraOptionsActivity mInjectee;

    public CameraOptionsModule(CameraOptionsActivity injectee) {
        this.mInjectee = injectee;
    }

}
