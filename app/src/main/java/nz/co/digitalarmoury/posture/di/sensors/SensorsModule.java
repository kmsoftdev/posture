package nz.co.digitalarmoury.posture.di.sensors;

import android.app.KeyguardManager;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import nz.co.digitalarmoury.posture.business.bus.Bus;
import nz.co.digitalarmoury.posture.business.prefs.PrefsRepo;
import nz.co.digitalarmoury.posture.business.sensor.AccelEventsHandler;
import nz.co.digitalarmoury.posture.business.sensor.AccelMagnEventsHandler;
import nz.co.digitalarmoury.posture.business.sensor.AccelMagnEventsReceiver;
import nz.co.digitalarmoury.posture.business.sensor.SensorEventsHandler;
import nz.co.digitalarmoury.posture.business.sensor.SensorEventsReceiver;
import nz.co.digitalarmoury.posture.business.sensor.TiltCalculator;
import nz.co.digitalarmoury.posture.business.sensor.TiltValidator;

/**
 * Created by Think on 03/10/2016.
 */

@Module
public class SensorsModule {

    @Provides
    @Singleton
    SensorManager provideSensorManager(Context context) {
        return (SensorManager) context.getSystemService(context.SENSOR_SERVICE);
    }

    @Provides
    @Singleton
    @Named("accelerometer")
    Sensor provideAccelerometer(SensorManager sensorManager) {
        return sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    @Provides
    @Singleton
    @Named("magnetometer")
    Sensor provideMagnetometer(SensorManager sensorManager) {
        Sensor defaultSensor = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        return defaultSensor != null ? defaultSensor : sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    @Provides
    @Singleton
    @Named("hasMagnetometer")
    boolean provideDeviceHasMagnetometer(SensorManager sensorManager) {
        if (sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD) != null) {
            return true;
        }
        else {
            return false;
        }

    }

    @Singleton
    @Provides
    SensorEventsReceiver provideSensorEventReceiver(@Named("hasMagnetometer") boolean deviceHasMagnetometer,
                                                    Bus bus,
                                                    SensorManager sensorManager,
                                                    @Named("accelerometer") Sensor accelerometer,
                                                    @Named("magnetometer") Sensor magnetometer,
                                                    SensorEventsHandler sensorEventsHandler,
                                                    PrefsRepo prefsRepo,
                                                    KeyguardManager keyguardManager) {
        if (deviceHasMagnetometer) {
            return new AccelMagnEventsReceiver(bus,
                                               sensorManager,
                                               accelerometer,
                                               magnetometer,
                                               sensorEventsHandler,
                                               prefsRepo,
                                               keyguardManager);
        }
        else
            return new AccelEventsReceiver(bus,
                                           sensorManager,
                                           accelerometer,
                                           sensorEventsHandler,
                                           prefsRepo,
                                           keyguardManager);

    }

    @Singleton
    @Provides
    SensorEventsHandler provideSensorEventsHandler(@Named("hasMagnetometer") boolean hasMagnetometer,
                                                   SensorManager sensorManger,
                                                   TiltCalculator tiltCalculator,
                                                   TiltValidator tiltValidator

    ) {
        if (hasMagnetometer) {
            return new AccelMagnEventsHandler(sensorManger, tiltCalculator, tiltValidator);
        }
        else return new AccelEventsHandler(tiltCalculator, tiltValidator);

    }

}
