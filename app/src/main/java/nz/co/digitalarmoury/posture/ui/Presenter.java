package nz.co.digitalarmoury.posture.ui;

import android.os.Bundle;

public abstract class Presenter<T extends BaseView> {
    protected T mView;

    public void setView(T view) {
        this.mView = view;
    }

    public void onResume() {

    }

    public void onPause() {
    }

    public void onSaveInstanceState(Bundle bundle) {

    }

    public void onViewStateRestored(Bundle bundle) {

    }

    public void onCreateView() {

    }

    public void onDestroyView() {
        this.mView = null;

    }

}
