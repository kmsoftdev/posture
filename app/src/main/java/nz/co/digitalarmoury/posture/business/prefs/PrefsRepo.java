package nz.co.digitalarmoury.posture.business.prefs;

import android.content.SharedPreferences;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class PrefsRepo {

    private static final String KEY_FACE_DETECTION = "USER_FACE_DETECTION_ENABLED";
    private static final String KEY_OVERLAY = "USE_OVERLAY";
    private static final String KEY_VIBRATION = "USE_VIBRATION";
    private static final String KEY_DIALOG = "SHOW_SYSTEM_DIALOG";
    private static final String KEY_DIALOG_DISMISSIBLE = "DIALOG_DISMISSIBLE";
    private static final String KEY_PICTURE_ROTATION = "PICTURE_ROTATION";

    private static final String KEY_TILT_LOW = "TILT_LOW";
    private static final String KEY_TILT_HIGH = "TILT_HIGH";
    private static final String KEY_OVERLAY_ALPHA_PERCENT = "OVERLAY_ALPHA_PERCENT";
    private static final String KEY_CHECK_SENSOR_READ_INTERVAL = "CHECK_SENSOR_READ_INTERVAL";
    private static final String KEY_ANGLE_FLAT = "ANGLE_FLAT";
    private final SharedPreferences mSharedPreferences;

    @Inject
    public PrefsRepo(SharedPreferences sharedPreferences) {
        mSharedPreferences = sharedPreferences;
    }

    public void setIsUsingFaceRecognition(boolean enabled) {
        mSharedPreferences.edit().putBoolean(KEY_FACE_DETECTION, enabled).apply();
    }

    public boolean getIsUsingFaceRecognition() {
        return mSharedPreferences.getBoolean(KEY_FACE_DETECTION, false);
    }

    public boolean getIsUsingOverlay() {
        return mSharedPreferences.getBoolean(KEY_OVERLAY, false);
    }

    public void setIsUsingOverlay(boolean activated) {
        mSharedPreferences.edit().putBoolean(KEY_OVERLAY, activated).apply();
    }

    public boolean isDialogDismissible() {
        return mSharedPreferences.getBoolean(KEY_DIALOG_DISMISSIBLE, false);
    }

    public void setDialogDismissible(boolean enabled) {
        mSharedPreferences.edit().putBoolean(KEY_DIALOG_DISMISSIBLE, enabled).apply();
    }

    public boolean getIsUsingVibration() {
        return mSharedPreferences.getBoolean(KEY_VIBRATION, false);
    }

    public void setIsUsingVibration(boolean useVibration) {
        mSharedPreferences.edit().putBoolean(KEY_VIBRATION, useVibration).apply();
    }

    public void setIsUsingDialog(boolean activated) {
        mSharedPreferences.edit().putBoolean(KEY_DIALOG, activated).apply();
    }

    public boolean getIsUsingDialog() {
        return mSharedPreferences.getBoolean(KEY_DIALOG, false);
    }

    public int getCameraRotationCorrection() {
        return mSharedPreferences.getInt(KEY_PICTURE_ROTATION, 0);
    }

    public void setCameraRotationCorrection(int rotationAngle) {
        mSharedPreferences.edit().putInt(KEY_PICTURE_ROTATION, rotationAngle).apply();
    }

    //development only

    public int getAngleLow() {
        return mSharedPreferences.getInt(KEY_TILT_LOW, 60);
    }

    public int getAngleHigh() {
        return mSharedPreferences.getInt(KEY_TILT_HIGH, 75);
    }

    public void setAngleLow(int newAngleLow) {
        mSharedPreferences.edit().putInt(KEY_TILT_LOW, newAngleLow).apply();
    }

    public void setAngleHigh(int newAngleHigh) {
        mSharedPreferences.edit().putInt(KEY_TILT_HIGH, newAngleHigh).apply();
    }

    public long getSensorInterval() {
        return mSharedPreferences.getLong(KEY_CHECK_SENSOR_READ_INTERVAL, 2);
    }

    public void setSensorInterval(long newSensorInterval) {
        mSharedPreferences.edit().putLong(KEY_CHECK_SENSOR_READ_INTERVAL, newSensorInterval).apply();
    }

    public float getOverlayPercent() {
        return (float) mSharedPreferences.getInt(KEY_OVERLAY_ALPHA_PERCENT, 25) / 100;
    }

    public void setOverlayAlphaPercent(int newAlpha) {
        mSharedPreferences.edit().putInt(KEY_OVERLAY_ALPHA_PERCENT, newAlpha).apply();
    }

    public void setAngleFlat(int newAngleFlat) {
        mSharedPreferences.edit().putInt(KEY_ANGLE_FLAT, newAngleFlat).apply();
    }

    public int getAngleFlat() {
        return mSharedPreferences.getInt(KEY_ANGLE_FLAT, 20);
    }
}
