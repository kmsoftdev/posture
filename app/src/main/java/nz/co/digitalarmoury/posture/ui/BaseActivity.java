package nz.co.digitalarmoury.posture.ui;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

import rx.subscriptions.CompositeSubscription;

public abstract class BaseActivity<T extends Presenter, S extends ViewDataBinding, U extends BaseView> extends AppCompatActivity {
    @Inject public T mPresenter;
    protected S mBinding;
    protected CompositeSubscription mCompositeSubscription;

    public BaseActivity() {
    }

    protected String getToolbarTitle() {
        return null;
    }

    protected abstract void injectDependencies();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectDependencies();
        mBinding = DataBindingUtil.setContentView(this, getLayout());
        mPresenter.setView(getPresenterView());
        mPresenter.onCreateView();
        customOnCreate(savedInstanceState);
        customOnCreate();
        customOnCreateView();
        customOnCreateView(savedInstanceState);
    }

    protected void customOnCreate(Bundle savedInstanceState) {

    }

    protected void customOnCreate() {

    }

    protected abstract
    @LayoutRes
    int getLayout();

    protected abstract U getPresenterView();

    protected void customOnCreateView() {

    }

    protected void customOnCreateView(Bundle savedInstanceState) {

    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroyView();
        if (mCompositeSubscription != null && !mCompositeSubscription.isUnsubscribed()) {
            mCompositeSubscription.clear();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mPresenter.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mPresenter.onViewStateRestored(savedInstanceState);
    }
}


