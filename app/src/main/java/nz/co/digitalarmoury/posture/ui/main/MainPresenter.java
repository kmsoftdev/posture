package nz.co.digitalarmoury.posture.ui.main;

import android.content.Context;

import javax.inject.Inject;

import nz.co.digitalarmoury.posture.business.bus.Bus;
import nz.co.digitalarmoury.posture.business.bus.ServiceStatusListener;
import nz.co.digitalarmoury.posture.business.faceRecognition.UserFaceRecognizer;
import nz.co.digitalarmoury.posture.business.prefs.PrefsRepo;
import nz.co.digitalarmoury.posture.ui.Presenter;
import nz.co.digitalarmoury.posture.ui.main.di.PerMain;
import nz.co.digitalarmoury.posture.ui.tiltMonitor.TiltMonitorService;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

@PerMain
public class MainPresenter extends Presenter<MainView> implements ServiceStatusListener {
    private final PrefsRepo mPrefsRepo;
    private final UserFaceRecognizer mUserFaceRecognizer;
    private final Context mContext;
    private final Bus mBus;
    private final CompositeSubscription subs;

    @Inject
    public MainPresenter(PrefsRepo prefsRepo,
                         UserFaceRecognizer userFaceRecognizer,
                         Context context,
                         Bus bus) {
        this.mPrefsRepo = prefsRepo;
        mUserFaceRecognizer = userFaceRecognizer;
        mContext = context;
        this.mBus = bus;
        subs = new CompositeSubscription();
    }

    @Override
    public void onCreateView() {
        super.onCreateView();
        mView.restoreUserOptions(
                mPrefsRepo.getIsUsingOverlay(),
                mPrefsRepo.getIsUsingDialog(),
                mPrefsRepo.isDialogDismissible(),
                mPrefsRepo.getIsUsingVibration(),
                mPrefsRepo.getIsUsingFaceRecognition()
        );
    }

    @Override
    public void onResume() {
        super.onResume();
        subs.add(mBus.subscribeToServiceStatusUpdates(this));
    }

    @Override
    public void onPause() {
        super.onPause();
        subs.clear();
    }

    public void setIsUsingOverlay(boolean activated) {
        mPrefsRepo.setIsUsingOverlay(activated);
    }

    public void showSystemDialog(boolean activated) {
        mPrefsRepo.setIsUsingDialog(activated);
        mView.setIsUsingSystemDialog(activated);
    }

    public void overlayAlphaChanged(CharSequence newAlphaString) {
        if (charSequenceEmptyOrNull(newAlphaString)) return;
        try {
            int newAlpha = Integer.parseInt(newAlphaString.toString());
            if (newAlpha < 0 || newAlpha > 94) {
                mView.showMessage("ignored value:" + newAlpha + ", range 0 - 95");
            }
            else {
                mPrefsRepo.setOverlayAlphaPercent(newAlpha);
            }
        } catch (NumberFormatException e) {
            //do nothing
        }
    }

    private boolean charSequenceEmptyOrNull(CharSequence newSensorIntervalString) {
        return newSensorIntervalString == null || newSensorIntervalString.length() < 1;
    }

    public void angleLowChanged(CharSequence newAngleLowString) {
        if (charSequenceEmptyOrNull(newAngleLowString)) return;

        try {
            int newAngleLow = Integer.parseInt(newAngleLowString.toString());
            mPrefsRepo.setAngleLow(newAngleLow);
        } catch (NumberFormatException e) {
            //do nothing
        }
    }

    public void angleHighChanged(CharSequence newAngleHighString) {
        if (charSequenceEmptyOrNull(newAngleHighString)) return;

        try {
            int newAngleHigh = Integer.parseInt(newAngleHighString.toString());
            mPrefsRepo.setAngleHigh(newAngleHigh);
        } catch (NumberFormatException e) {
            //do nothin
        }
    }

    public void angleFlatChanged(CharSequence newAngleFlatString) {
        if (charSequenceEmptyOrNull(newAngleFlatString)) return;

        try {
            int newAngleFlat = Integer.parseInt(newAngleFlatString.toString());
            mPrefsRepo.setAngleFlat(newAngleFlat);
        } catch (NumberFormatException e) {
            //do nothin
        }
    }

    public void sensorIntervalChanged(CharSequence newSensorIntervalString) {
        if (charSequenceEmptyOrNull(newSensorIntervalString)) return;

        try {
            long newSensorInterval = Long.parseLong(newSensorIntervalString.toString());
            mPrefsRepo.setSensorInterval(newSensorInterval);
        } catch (NumberFormatException e) {
            //do nothing
        }
    }

    public void userFaceDetection(boolean enabled) {
        mPrefsRepo.setIsUsingFaceRecognition(enabled);
    }

    public void takeFrontCameraPic() {
        mUserFaceRecognizer.getCurrentFrontCameraImage()
                           .subscribeOn(Schedulers.io())
                           .observeOn(AndroidSchedulers.mainThread())
                           .subscribe(bitmap -> {
                               mView.setFrontCameraPreview(bitmap);
                           }, Throwable::printStackTrace);

    }

    public void testFaceRecognition() {
        mUserFaceRecognizer.getCurrentFrontCameraImage()
                           .subscribeOn(Schedulers.io())
                           .observeOn(AndroidSchedulers.mainThread())
                           .subscribe(bitmap -> {
                               mView.setFaceRecognitionPreview(bitmap);
                               mUserFaceRecognizer.getNumberOfFacesDetected(bitmap)
                                                  .subscribeOn(Schedulers.io())
                                                  .observeOn(AndroidSchedulers.mainThread())
                                                  .subscribe(facesDetected -> {
                                                      mView.markFacesDetected(facesDetected);
                                                  }, throwable -> {
                                                      mView.showMessage(throwable.getMessage());
                                                  });

                           }, throwable -> {
                               mView.showMessage(throwable.getMessage());
                           });

    }

    public void dialogStrictMode(boolean activated) {
        mPrefsRepo.setDialogDismissible(activated);
    }

    public void useVibration(boolean activated) {
        mPrefsRepo.setIsUsingVibration(activated);
    }

    @Override
    public void serviceStatusUpdated(boolean activated) {
        mView.serviceStatusUpdated(activated);
    }

    public void startService(boolean isCurrentlyActivated) {
        mContext.startService(TiltMonitorService.getSetActivatedIntent(mContext, !isCurrentlyActivated));
        if (!isCurrentlyActivated) {
            mView.finish();
        }
    }
}