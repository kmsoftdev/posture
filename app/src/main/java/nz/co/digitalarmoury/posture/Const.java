package nz.co.digitalarmoury.posture;

/**
 * Created by Think on 04/10/2016.
 */

public class Const {
    public static final float DEVICE_FLAT_ANGLE = 20;
    public static final String ANGLE_MONITOR = "AngleMonitor";
}
