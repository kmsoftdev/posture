package nz.co.digitalarmoury.posture.ui.settings;

import nz.co.digitalarmoury.posture.business.bus.Bus;
import nz.co.digitalarmoury.posture.business.bus.ServiceStatusListener;
import nz.co.digitalarmoury.posture.business.prefs.PrefsRepo;
import nz.co.digitalarmoury.posture.ui.Presenter;
import nz.co.digitalarmoury.posture.ui.settings.di.PerSettings;

import javax.inject.Inject;

import rx.subscriptions.CompositeSubscription;

@PerSettings
public class SettingsPresenter extends Presenter<SettingsView> implements ServiceStatusListener {
    private final PrefsRepo mPrefsRepo;
    private final Bus mBus;
    private final CompositeSubscription subs;

    @Inject
    public SettingsPresenter(PrefsRepo prefsRepo, Bus bus) {

        mPrefsRepo = prefsRepo;
        mBus = bus;
        subs = new CompositeSubscription();
    }

    @Override
    public void onCreateView() {
        super.onCreateView();
        mView.restoreUserSettings(
                mPrefsRepo.getIsUsingOverlay(),
                mPrefsRepo.getIsUsingDialog(),
                mPrefsRepo.isDialogDismissible(),
                mPrefsRepo.getIsUsingVibration(),
                mPrefsRepo.getIsUsingFaceRecognition()

        );
    }

    @Override
    public void onResume() {
        super.onResume();
        subs.add(mBus.subscribeToServiceStatusUpdates(this));
    }

    @Override
    public void onPause() {
        super.onPause();
        subs.clear();
    }

    @Override
    public void serviceStatusUpdated(boolean mActivated) {
        mView.serviceStatusUpdated(mActivated);
    }

    public void setOverlay(boolean activated) {
        mPrefsRepo.setIsUsingOverlay(activated);
    }

    public void setDialog(boolean activated) {
        mPrefsRepo.setIsUsingDialog(activated);
    }

    public void setStrictMode(boolean activated) {
        mPrefsRepo.setDialogDismissible(activated);
    }

    public void setVibration(boolean activated) {
        mPrefsRepo.setIsUsingVibration(activated);
    }

    public void setFaceRecognition(boolean activated) {
        mPrefsRepo.setIsUsingFaceRecognition(activated);
    }

}
