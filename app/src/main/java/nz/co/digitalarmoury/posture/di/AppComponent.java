package nz.co.digitalarmoury.posture.di;

import nz.co.digitalarmoury.posture.di.analytics.AnalyticsModule;
import nz.co.digitalarmoury.posture.di.faceRecognition.FaceRecognitionModule;
import nz.co.digitalarmoury.posture.di.sensors.SensorsModule;
import nz.co.digitalarmoury.posture.di.vibration.VibrationModule;
import nz.co.digitalarmoury.posture.ui.cameraOptions.di.CameraOptionsComponent;
import nz.co.digitalarmoury.posture.ui.cameraOptions.di.CameraOptionsModule;
import nz.co.digitalarmoury.posture.ui.learnMore.di.LearnMoreComponent;
import nz.co.digitalarmoury.posture.ui.learnMore.di.LearnMoreModule;
import nz.co.digitalarmoury.posture.ui.main.di.MainComponent;
import nz.co.digitalarmoury.posture.ui.main.di.MainModule;
import nz.co.digitalarmoury.posture.ui.settings.di.SettingsComponent;
import nz.co.digitalarmoury.posture.ui.settings.di.SettingsModule;
import nz.co.digitalarmoury.posture.ui.tiltMonitor.di.TiltMonitorComponent;
import nz.co.digitalarmoury.posture.ui.tiltMonitor.di.TiltMonitorModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class,
        SensorsModule.class,
        FaceRecognitionModule.class,
        VibrationModule.class,
        AnalyticsModule.class})
public interface AppComponent {

    MainComponent sub(MainModule mainModule);

    TiltMonitorComponent sub(TiltMonitorModule tiltMonitorModule);

    LearnMoreComponent sub(LearnMoreModule learnMoreModule);

    SettingsComponent sub(SettingsModule settingsModule);

    CameraOptionsComponent sub(CameraOptionsModule cameraOptionsModule);
}
