package nz.co.digitalarmoury.posture.model.camera;

/**
 * Created by Think on 06/10/2016.
 */

public class ByteArrayHolder {
    private byte[] bitmapData;

    public ByteArrayHolder(byte[] bitmapData) {
        this.bitmapData = bitmapData;
    }

    public byte[] getBitmapData() {
        return this.bitmapData;
    }
}
