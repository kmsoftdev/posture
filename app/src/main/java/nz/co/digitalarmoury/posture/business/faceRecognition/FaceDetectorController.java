package nz.co.digitalarmoury.posture.business.faceRecognition;

import android.graphics.Bitmap;
import android.util.SparseArray;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;
import rx.subscriptions.Subscriptions;

@Singleton
public class FaceDetectorController {

    private final FaceDetector.Builder mFaceDetectorBuilder;

    @Inject
    public FaceDetectorController(FaceDetector.Builder faceDetector) {

        mFaceDetectorBuilder = faceDetector;
    }

    public Observable<Integer> facesDetected(Bitmap bitmap) {
        return Observable.create(subscriber -> {
            FaceDetector detector = mFaceDetectorBuilder.build();
            Frame frame = new Frame.Builder().setBitmap(bitmap).build();
            SparseArray<Face> faces = detector.detect(frame);
            if (faces != null && faces.size() > 0) subscriber.onNext(faces.size());
            else subscriber.onNext(0);
            subscriber.onCompleted();
            subscriber.add(Subscriptions.create(detector::release));
        });


    }
}
