package nz.co.digitalarmoury.posture.di.faceRecognition;

import android.content.Context;

import com.google.android.gms.vision.face.FaceDetector;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Think on 06/10/2016.
 */

@Module
public class FaceDetectorModule {

    @Singleton
    @Provides
    FaceDetector.Builder
    provideFaceDetector(Context context) {
        return new FaceDetector.Builder(context)
                .setTrackingEnabled(false)
                .setProminentFaceOnly(true);
    }
}


