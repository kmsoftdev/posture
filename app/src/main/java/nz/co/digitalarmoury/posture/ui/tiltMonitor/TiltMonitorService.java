package nz.co.digitalarmoury.posture.ui.tiltMonitor;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.RemoteViews;

import nz.co.digitalarmoury.posture.App;
import nz.co.digitalarmoury.posture.R;
import nz.co.digitalarmoury.posture.ui.alertDialogView.AlertDialogView;
import nz.co.digitalarmoury.posture.ui.learnMore.LearnMoreActivity;
import nz.co.digitalarmoury.posture.ui.main.MainActivity;
import nz.co.digitalarmoury.posture.ui.settings.SettingsActivity;
import nz.co.digitalarmoury.posture.ui.tiltMonitor.di.TiltMonitorModule;

import javax.inject.Inject;

public class TiltMonitorService extends Service implements TiltMonitorView, AlertDialogView.EventListener {
    private static final String KEY_ACTIVATE_TILT_STREAMING = "activate";
    private static final int NOTIFICATION_ID = 1340;
    private static final int REQUEST_STOP_BACKGROUND_MODE = 1341;
    private static final int REQUEST_SETTINGS = 1342;

    @Inject TiltMonitorPresenter mPresenter;
    @Inject WindowManager mWindowManager;
    private int mStartId;
    private View mOverlayView;
    private WindowManager.LayoutParams mOverlayParams;
    private AlertDialogView mAlertDialogView;
    private WindowManager.LayoutParams mAlertDialogParams;
    private Notification notification;

    public static Intent getSetActivatedIntent(Context context, boolean activate) {
        Intent intent = new Intent(context, TiltMonitorService.class);
        intent.putExtra(KEY_ACTIVATE_TILT_STREAMING, activate);
        return intent;
    }

    public static Intent getShowSettingsIntent(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        return intent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ((App) getApplication()).getAppComponent().sub(new TiltMonitorModule(this))
                                .inject(this);
        mPresenter.setView(this);
        mPresenter.onCreateView();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        this.mStartId = startId;
        if (intent != null && intent.getExtras() != null) {
            if (intent.getExtras().containsKey(KEY_ACTIVATE_TILT_STREAMING)) {
                mPresenter.setTiltDetection(intent.getBooleanExtra(KEY_ACTIVATE_TILT_STREAMING, false));
            }

        }

        return START_STICKY;

    }

    @Override
    public void setBackgroundMode(boolean activated) {
        if (activated) {
            notification = getNotification(false);
            startForeground(NOTIFICATION_ID, notification);
        }
        else {
            stopForeground(true);
            stopSelf(mStartId);
        }
    }

    private Notification getNotification(boolean showError) {
        RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.notif_tilt_service);

        PendingIntent stopService = PendingIntent.getService(getApplicationContext(),
                                                             REQUEST_STOP_BACKGROUND_MODE,
                                                             getSetActivatedIntent(getApplicationContext(),
                                                                                   false),
                                                             PendingIntent.FLAG_CANCEL_CURRENT);
        PendingIntent showSettings = PendingIntent.getActivity(getApplicationContext(),
                                                               REQUEST_SETTINGS,
                                                               SettingsActivity.createIntent(getApplicationContext()),
                                                               PendingIntent.FLAG_CANCEL_CURRENT);

        remoteViews.setOnClickPendingIntent(R.id.stopBackgroundMode, stopService);
        remoteViews.setOnClickPendingIntent(R.id.showSettings, showSettings);

        return notification = new NotificationCompat.Builder(getApplicationContext())
                .setCustomBigContentView(remoteViews)
                .setCustomContentView(remoteViews)
                .setSmallIcon(showError ? R.drawable.ic_close : R.drawable.ic_camera)
                .build();
    }

    private void createAlertViewIfNew(boolean strictModeEnabled) {
        if (mAlertDialogView == null) {
            mAlertDialogView = new AlertDialogView(getApplicationContext(), strictModeEnabled);
            mAlertDialogView.setEventListener(this);
        }
        else {
            mAlertDialogView.setDialogDismissible(strictModeEnabled);
        }
        if (mAlertDialogParams == null) {
            mAlertDialogParams = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.TYPE_TOAST,
                    WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN,
                    PixelFormat.TRANSPARENT);

            mAlertDialogParams.gravity = Gravity.CENTER;
            mAlertDialogParams.width = getResources().getDimensionPixelSize(R.dimen.alert_dialog_width);
            mAlertDialogParams.height = getResources().getDimensionPixelSize(R.dimen.alert_dialog_height);
        }
    }

    @Override
    public void showAngleIncorrectOverlay(boolean show, float overlayAlpha) {
        if (show) {
            addOverlay(overlayAlpha);
        }
        else {
            removeOverlay();
        }

    }

    private void addOverlay(float overlayAlpha) {
        if (mOverlayView != null && mOverlayView.isShown()) {
            return;
        }
        createOverlayViewIfNew(overlayAlpha);

        mWindowManager.addView(mOverlayView, mOverlayParams);
    }

    private void createOverlayViewIfNew(float overlayAlpha) {
        if (mOverlayView == null) {
            mOverlayView = new View(this);
            mOverlayView.setBackgroundColor(Color.RED);
            mOverlayView.setAlpha(overlayAlpha);

        }
        if (mOverlayParams == null) {
            mOverlayParams = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.TYPE_TOAST,
                    WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM
                            | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                            | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
                            | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN,
                    PixelFormat.TRANSPARENT);
        }
    }

    private void removeOverlay() {
        if (mOverlayView != null && mOverlayView.getParent() != null) {
            mWindowManager.removeView(mOverlayView);
        }
    }

    @Override
    public void setDialogDismissible(boolean dialogStrictMode) {
        createAlertViewIfNew(dialogStrictMode);
    }

    @Override
    public void updateNotification(boolean showError) {
        notification = getNotification(showError);
        NotificationManager notificationManagerCompat = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManagerCompat.notify(NOTIFICATION_ID, notification);
    }

    @Override
    public void showAngleIncorrectDialog(boolean show, boolean dialogDismissible) {
        if (show) {
            addAlertView(dialogDismissible);
        }
        else {
            removeAlertDialog();
        }

    }

    private void addAlertView(boolean strictMode) {
        createAlertViewIfNew(strictMode);
        if (!mAlertDialogView.isShowing()) {
            mWindowManager.addView(mAlertDialogView, mAlertDialogParams);
        }
    }

    @Override
    public void closeAlertView() {
        removeAlertDialog();
    }

    private void removeAlertDialog() {
        if (mAlertDialogView != null && mAlertDialogView.isShowing()) {
            mWindowManager.removeView(mAlertDialogView);
        }
    }

    @Override
    public void showLearnMore() {
        removeAlertDialog();
        startActivity(LearnMoreActivity.createIntent(getApplicationContext()));
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroyView();
        removeOverlay();
        removeAlertDialog();
    }
}