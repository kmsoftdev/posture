package nz.co.digitalarmoury.posture.ui.tiltMonitor;

import nz.co.digitalarmoury.posture.ui.BaseView;

public interface TiltMonitorView extends BaseView {
    void setBackgroundMode(boolean activated);

    void showAngleIncorrectOverlay(boolean show, float overlayAlpha);

    void showAngleIncorrectDialog(boolean show, boolean dialogDismissible);

    void setDialogDismissible(boolean dialogStrictMode);

    void updateNotification(boolean showError);
}