package nz.co.digitalarmoury.posture.business.sensor;

import android.hardware.SensorManager;

import javax.inject.Inject;
import javax.inject.Singleton;

import nz.co.digitalarmoury.posture.model.sensors.SensorEventsWrapper;
import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by Think on 03/10/2016.
 */

@Singleton
public class AccelMagnEventsHandler implements SensorEventsHandler {
    private final SensorManager mSensorManager;
    private final TiltCalculator mTiltCalculator;
    private final TiltValidator mTiltValidator;
    private final float[] mAccelerometerReading = new float[3];
    private final float[] mMagnetometerReading = new float[3];
    private final float[] mRotationMatrix = new float[9];
    private final float[] mOrientationAngles = new float[3];

    @Inject
    public AccelMagnEventsHandler(SensorManager sensorManager,
                                  TiltCalculator tiltCalculator,
                                  TiltValidator tiltValidator) {

        mSensorManager = sensorManager;
        mTiltCalculator = tiltCalculator;
        mTiltValidator = tiltValidator;
    }

    @Override
    public Observable<Boolean> validateCurrentTilt(SensorEventsWrapper sensorEvents) {
        return getCurrentTilt(sensorEvents)
                .flatMap(this::validateTilt)
                .subscribeOn(Schedulers.computation());
    }

    private Observable<Float> getCurrentTilt(SensorEventsWrapper sensorEvents) {
        return Observable.defer(() -> {

            System.arraycopy(sensorEvents.getAccelerometerSensorEvent().values, 0, mAccelerometerReading, 0, mAccelerometerReading.length);

            System.arraycopy(sensorEvents.getMagnetometerSensorEvent().values, 0, mMagnetometerReading, 0, mMagnetometerReading.length);

            mSensorManager.getRotationMatrix(mRotationMatrix, null, mAccelerometerReading, mMagnetometerReading);

            mSensorManager.getOrientation(mRotationMatrix, mOrientationAngles);

            return mTiltCalculator.getCurrentTilt(mOrientationAngles);
        });
    }

    private Observable<Boolean> validateTilt(float currentTilt) {
        return mTiltValidator.validateAngle(currentTilt);
    }

}
