package nz.co.digitalarmoury.posture.utils;

import android.databinding.BindingAdapter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Think on 11/10/2016.
 */

public class DataBindingHelper {
    @BindingAdapter({"bind:filter"})
    public static void loadImage(ImageView imageView, int color) {
        imageView.setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_ATOP));
    }


    @BindingAdapter({"bind:drawableTopTint"})
    public static void setDrawableTopTint(TextView textView, int color) {
        if (textView.getCompoundDrawables()[1] != null) {
            textView.getCompoundDrawables()[1].setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_ATOP));

        }
    }
}
