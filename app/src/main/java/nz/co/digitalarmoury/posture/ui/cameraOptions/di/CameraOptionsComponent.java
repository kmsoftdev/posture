package nz.co.digitalarmoury.posture.ui.cameraOptions.di;

import nz.co.digitalarmoury.posture.ui.cameraOptions.CameraOptionsActivity;

import dagger.Subcomponent;

@PerCameraOptions
@Subcomponent(modules = CameraOptionsModule.class)
public interface CameraOptionsComponent {
    void inject(CameraOptionsActivity injectee);
}


