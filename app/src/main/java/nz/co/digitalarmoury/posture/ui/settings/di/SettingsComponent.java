package nz.co.digitalarmoury.posture.ui.settings.di;

import nz.co.digitalarmoury.posture.ui.settings.SettingsActivity;

import dagger.Subcomponent;

@PerSettings
@Subcomponent(modules = SettingsModule.class)
public interface SettingsComponent {
    void inject(SettingsActivity injectee);
}


