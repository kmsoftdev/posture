package nz.co.digitalarmoury.posture.ui.learnMore.di;

import nz.co.digitalarmoury.posture.ui.learnMore.LearnMoreActivity;

import dagger.Subcomponent;

@PerLearnMore
@Subcomponent(modules = LearnMoreModule.class)
public interface LearnMoreComponent {
    void inject(LearnMoreActivity injectee);
}


